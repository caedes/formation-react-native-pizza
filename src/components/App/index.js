import React, { Component } from 'react';

import { AppWrapper } from './styles';
import Header from '../Header';
import TabNavigation from '../TabNavigation';

export default class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Header title="Pizza Shop" />
        <TabNavigation />
      </AppWrapper>
    );
  }
}
