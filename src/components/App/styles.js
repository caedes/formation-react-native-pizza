import glamorous from 'glamorous-native';

import { geyser } from '../../styles/colors';

// eslint-disable-next-line import/prefer-default-export
export const AppWrapper = glamorous.view({
  backgroundColor: geyser,
  flex: 1,
});
