import React, { PureComponent } from 'react';
import { WebView } from 'react-native';

const cartUrl = 'https://www.dominos.fr/la-carte/nos-pizzas';

export default class CartWebviewScreen extends PureComponent {
  render() {
    return <WebView source={{ uri: cartUrl }} />;
  }
}
