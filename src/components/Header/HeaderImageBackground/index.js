import { ImageBackground } from 'react-native';
import { node } from 'prop-types';
import React, { PureComponent } from 'react';

import headerImage from '../../../assets/header_pizz.png';
import styles from './styles';

export default class HeaderImageBackground extends PureComponent {
  static propTypes = {
    children: node.isRequired,
  };

  render() {
    return (
      <ImageBackground source={headerImage} style={styles.headerImageBackground}>
        {this.props.children}
      </ImageBackground>
    );
  }
}
