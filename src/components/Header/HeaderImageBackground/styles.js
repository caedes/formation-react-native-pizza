import { StyleSheet } from 'react-native';
import { Constants } from 'expo';

export default StyleSheet.create({
  headerImageBackground: {
    height: 80,
    marginTop: Constants.statusBarHeight,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
