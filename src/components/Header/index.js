import { string } from 'prop-types';
import React, { PureComponent } from 'react';

import HeaderImageBackground from './HeaderImageBackground';
import { HeaderTitle } from './styles';

export default class Header extends PureComponent {
  static propTypes = {
    title: string.isRequired,
  };

  render() {
    return (
      <HeaderImageBackground>
        <HeaderTitle>{this.props.title}</HeaderTitle>
      </HeaderImageBackground>
    );
  }
}
