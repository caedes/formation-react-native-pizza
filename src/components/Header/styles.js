import glamorous from 'glamorous-native';

import { alphaBlack, white } from '../../styles/colors';

// eslint-disable-next-line import/prefer-default-export
export const HeaderTitle = glamorous.text({
  color: white,
  fontSize: 30,
  fontWeight: 'bold',
  textShadowColor: alphaBlack,
  textShadowOffset: {
    width: -1,
    height: -1,
  },
});
