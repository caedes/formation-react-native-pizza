import { ActivityIndicator } from 'react-native';
import React, { PureComponent } from 'react';

import { LoaderWrapper } from './styles';
import { dodgerBlue } from '../../styles/colors';

export default class Loader extends PureComponent {
  render() {
    return (
      <LoaderWrapper>
        <ActivityIndicator color={dodgerBlue} size="large" />
      </LoaderWrapper>
    );
  }
}
