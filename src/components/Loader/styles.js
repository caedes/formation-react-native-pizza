import glamorous from 'glamorous-native';

// eslint-disable-next-line import/prefer-default-export
export const LoaderWrapper = glamorous.view({
  marginTop: 30,
});
