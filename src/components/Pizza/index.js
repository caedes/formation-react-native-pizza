import { string } from 'prop-types';
import React, { PureComponent } from 'react';

import { PizzaImage, PizzaTitle, PizzaWrapper } from './styles';
import capitalize from '../../lib/capitalize';

export default class Pizza extends PureComponent {
  static propTypes = {
    title: string.isRequired,
    imageUri: string.isRequired,
  };

  render() {
    const title = capitalize(this.props.title);

    return (
      <PizzaWrapper>
        <PizzaImage source={{ uri: this.props.imageUri }} />
        <PizzaTitle>{title}</PizzaTitle>
      </PizzaWrapper>
    );
  }
}
