import glamorous from 'glamorous-native';

import { tundora, white } from '../../styles/colors';

export const PizzaTitle = glamorous.text({
  color: tundora,
  fontSize: 18,
  paddingLeft: 30,
  paddingTop: 22,
});

export const PizzaImage = glamorous.image({
  borderBottomLeftRadius: 0,
  borderBottomRightRadius: 0,
  borderRadius: 5,
  height: 150,
});

export const PizzaWrapper = glamorous.view({
  backgroundColor: white,
  borderRadius: 5,
  height: 220,
  marginBottom: 30,
  marginLeft: 20,
  marginRight: 20,
  elevation: 10,
});
