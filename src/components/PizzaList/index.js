import { arrayOf, object } from 'prop-types';
import React, { PureComponent } from 'react';
import * as Animatable from 'react-native-animatable';

import Pizza from '../Pizza';
import { FlatListWrapper } from './styles';

export default class PizzaList extends PureComponent {
  static propTypes = {
    pizzas: arrayOf(object),
  };

  static defaultProps = {
    pizzas: [],
  };

  pizzaKeyExtractor = pizza => pizza.id;

  renderPizza = ({ item }) => (
    <Animatable.View animation="fadeInDown" duration={400} easing="ease-out-quint">
      <Pizza title={item.title} imageUri={item.image.url} />
    </Animatable.View>
  );

  render() {
    return (
      <FlatListWrapper
        data={this.props.pizzas}
        renderItem={this.renderPizza}
        keyExtractor={this.pizzaKeyExtractor}
      />
    );
  }
}
