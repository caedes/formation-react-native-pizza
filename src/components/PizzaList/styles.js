import glamorous from 'glamorous-native';

// eslint-disable-next-line import/prefer-default-export
export const FlatListWrapper = glamorous.flatList({
  paddingTop: 20,
  paddingBottom: 30,
});
