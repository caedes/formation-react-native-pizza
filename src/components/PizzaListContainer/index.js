import axios from 'axios';
import React, { PureComponent } from 'react';

import PizzaList from '../PizzaList';
import Loader from '../Loader';

export default class PizzaListContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      pizzas: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://pizza-rapid-store.surge.sh/api/pizzas.json')
      .then(response => response.data)
      .then(this.setPizzas);
  }

  setPizzas = (pizzas) => {
    this.setState({
      pizzas,
    });
  };

  render() {
    if (this.state.pizzas.length === 0) return <Loader />;

    return <PizzaList pizzas={this.state.pizzas} />;
  }
}
