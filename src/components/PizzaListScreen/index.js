import React, { PureComponent } from 'react';
import PizzaListContainer from '../PizzaListContainer';

export default class PizzaListScreen extends PureComponent {
  render() {
    return <PizzaListContainer />;
  }
}
