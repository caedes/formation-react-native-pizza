import { TabNavigator } from 'react-navigation';

import PizzaListScreen from '../PizzaListScreen';
import CartWebviewScreen from '../CartWebviewScreen';

export default TabNavigator(
  {
    'pizza list': {
      screen: PizzaListScreen,
    },
    cart: {
      screen: CartWebviewScreen,
    },
  },
  {
    swipeEnabled: true,
    tabBarPosition: 'bottom',
  },
);
