/* eslint-disable max-len */
export default [
  {
    id: 'guid2725673652',
    title: 'CHEESE & TOMATO',
    description: 'Base sauce tomate, mozzarella, tomates fraîches',
    price: 1099,
    currency: '€',
    image: {
      url: 'https://kingersheim.papajohns.fr/binary_resources/10169165',
    },
  },
  {
    id: 'guid387467836478364',
    title: 'FETA & MIEL - LARDONS',
    description:
      'Base crème fraîche légère, mozzarella, lardons fumés, fromage feta, oignons rouges, miel doux',
    price: 1199,
    currency: '€',
    image: {
      url: 'https://kingersheim.papajohns.fr/binary_resources/10277495',
    },
  },
  {
    id: 'guid89744689',
    title: 'SPICY TUNA',
    description:
      'Base sauce tomate, mozzarella, thon, champignons, oignons, olives vertes, spirale de sauce Aji verte Salsa',
    price: 1250,
    currency: '€',
    image: {
      url: 'https://kingersheim.papajohns.fr/binary_resources/10279450',
    },
  },
];
