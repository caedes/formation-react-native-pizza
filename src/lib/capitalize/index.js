import { compose, join, juxt, toUpper, toLower, head, tail } from 'ramda';

export default compose(
  join(''),
  juxt([compose(toUpper, head), compose(toLower, tail)]),
);
