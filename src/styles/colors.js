// BLACK
export const tundora = '#424242';

// BLUE
export const dodgerBlue = '#448aff';

// WHITE
export const geyser = '#dbdfe8';
export const white = '#fff';

// TRANSPARENT
export const alphaBlack = 'rgba(0, 0, 0, 0.5)';
